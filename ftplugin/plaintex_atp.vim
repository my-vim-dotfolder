" Maintainer:	Marcin Szamotulski
" Note:		This file is a part of Automatic Tex Plugin for Vim.
" URL:		https://launchpad.net/automatictexplugin

" b:atp_TexFlavor will be set to plaintex automatically
runtime ftplugin/tex_atp.vim
