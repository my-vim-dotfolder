
" Vim compiler file
" Language:		Prolog

if exists("current_compiler")
  finish
endif
let current_compiler = "prolog"

if exists(":CompilerSet") != 2		" older Vim always used :setlocal
  command -nargs=* CompilerSet setlocal <args>
endif

let s:cpo_save = &cpo
set cpo-=C

CompilerSet makeprg=swipl\ -s\ %\ -g\ \'halt\'

CompilerSet errorformat=
	\%W%>Warning:\ %f:%l:,
	\ERROR:\ %f:%l:%c:\ %m,
	\%Z%m

let &cpo = s:cpo_save
unlet s:cpo_save

" vim: nowrap sw=2 sts=2 ts=8:
