" Vim syntax file
" Language:	   stql Syntax file
" Maintainer:  Dawid Chemloul (lahouari@o2.pl)
" Last Change: Mon Oct 03 18:43:12 CET 2005

"  Syntax definition (*.sdf)|*.sdf
"
" For version 5.x: Clear all syntax items
" For version 6.x: Quit when a syntax file was already loaded
if version < 600
	syntax clear
elseif exists("b:current_syntax")
	finish
endif


syn region stqldefComment start="#" end="\_$"
syn region stqldefString start=+"+ skip=+\\"+ end=+"+

syn match stqldefFields "\(\<COMPLEX_FIELD\>\|\<DATA_FIELD\>\|\<FIELD\>\|\<RULE_FIELD\>\|\<USER_FIELD\>\)"

syn match stqldefConst "\(\<RESERVED\>\|\<CONST_DEF\>\|\<CONST_VALUE\>\)"

syn match stqldefType "\(\<CURRENT_OFFSET\>\|\<RAW_DATA\>\)"

syn match stqldefControlKWord1 "\(\<IF\>\|\<IFF\>\|\<FOR\>\|\<ELSE\>\)"
syn match stqldefControlKWord2 "\(\<BEGIN\>\|\<END\>\|\<BEGIN_FOR\>\|\<END_FOR\>\|\<BEGIN_IF\>\|\<END_IF\>\|\<BEGIN_RULE\>\|\<END_RULE\>\)"

syn match stqldefControlSpecial "\(\<FOR_PLUS\>\|\<FOR_MINUS\>\|\<IF_OR\>\|\<IF_AND\>\|\<IFF_OR\>\|\<IFF_AND\>\)"

syn match sqldefSpecialKWord "\(\<REWRITE\>\|\<PRE_PARSE\>\|\<QUERY\>\|\<GROUP\>\|\<FIRST_IN_GROUP\>\|\<LAST_IN_GROUP\>\|\<CONTINUITY_CHECK\>\|\<EMPTY\>\|\<VALID\>\|\<VALID_COLLECT\>\|\<IS_CRC\>\|\<VALID_CRC\>\)"

syn match stqldefOperator "\(\<EQ\>\|\<GT\>\|\<GE\>\|\<LT\>\|\<LE\>\|\<MASK_EQ\>\|\<EQ_CRC\>\|\<NOT\>\|\<MASK_NEQ\>\|\<NEQ\>\)"

syn match stqldefAtribute     "\(\<UNKNOWN_ATTRIBUTE\>\|\<EXIST_FIELD\>\|\<EXIST_ATTRIB\>\|\<RAW_DATA_ATTRIB\>\|\<SYNTAX_SUBTYPE_ID\>\|\<ID\>\|\<ZERO_MEANS_LONG\>\|\<BYTE_COUNTER\>\|\<ITER_COUNTER\>\|\<PAYLOAD_DATA\>\|\<MAX_LENGTH\>\|\<GROUP_ID\>\|\<CHILD_LINK\>\|\<COMP\>\|\<COUNTER\>\|\<LAST_COUNTER\>\|\<SET\>\|\<DISTINCT\>\|\<COUNT\>\|\<ALL\>\|\<STATIC_IDENT\>\|\<RUNTIME_IDENT\>\|\<MIN_LENGTH\>\|\<GROUP_KEY\>\|\<CRC32\>\|\<CRC_CHECK\>\)"

syn match stqldefSpecialAtribute "\(\<LENGTH\>\|\<MAX_LENGTH\>\|\<NAME\>\|\<HIDDEN\>\)"

syn match stqldefLoops "\(\<DATA_LOOP\>\|\<OBJECT_LOOP\>\|\<COMPLEX_LOOP\>\)"	

syn match stqldefVSSKeyWord "\(\<VSS_GROUP\>\|\<VSS_LEAF_TITLE_1\>\|\<VSS_LEAF_TITLE_2\>\|\<VSS_LEAF_TITLE_3\>\|\<VSS_SET_EXTRA_GROUPING\>\|\<VSS_TEXT\>\)"

syn match stqldefNumber "\s\<[0-9]*\>\s"hs=s+1,he=e-1

syn match stqldefHex "\s\<0x\c[0-9a-f]*\>\s"hs=s+1,he=e-1

if version >= 508 || !exists("did_stqldef_syn_inits")
  if version < 508
    let did_stqldef_syn_inits = 1
    command -nargs=+ HiLink hi link <args>
  else
    command -nargs=+ HiLink hi def link <args>
  endif

	HiLink stqldefComment	 Comment
	
	HiLink stqldefString     String

	HiLink stqldefFields     Type

	HiLink stqldefConst      Constant
	
	HiLink stqldefType       Type

	HiLink stqldefControlKWord1     Special

	HiLink stqldefControlKWord2     Identifier

	HiLink stqldefControlSpecial    Statement
	
	HiLink stqldefVSSKeyWord        Macro

	HiLink sqldefSpecialKWord       PreProc

	HiLink stqldefAtribute          String

	HiLink stqldefOperator          Operator
	
	HiLink stqldefLoops             Identifier

	HiLink stqldefHex               Type 
	HiLink stqldefNumber            Type

	HiLink stqldefSpecialAtribute   Error
  delcommand HiLink
endif

let b:current_syntax = "stqldef"	
	
