" Vim syntax file
" Language:	Quickfix window
" Maintainer:	Bram Moolenaar <Bram@vim.org>: 
" Updater:      Maciej Łaszcz   
" Last change:	2010 oct 26

" Quit when a syntax file was already loaded
if exists("b:current_syntax")
  finish
endif

" Standard highlighting
syn match	qfFileName	"^[^|]*" nextgroup=qfSeparator
syn match	qfSeparator	"|" nextgroup=qfLineNr contained
syn match	qfLineNr	"[^|]*" nextgroup=qfMessage contained
syn match	qfMessage	".*$" contained contains=qfError,qfQuote
syn match	qfError		"error" contained

" gcc messages help
syn match	qfQuote		"‘[^’]*" contained "contains=qfBrackets
"syn match	qfBrackets	"<[^>]*" contained contains=qfBrackets


" The default highlighting.
hi def link qfFileName	Directory
hi def link qfLineNr	LineNr
hi def link qfError	Error

hi def link qfQuote	Type

let b:current_syntax = "qf"

" vim: ts=8
