" This scheme was created by CSApproxSnapshot
" on Sat, 03 Sep 2011

hi clear
if exists("syntax_on")
    syntax reset
endif

if v:version < 700
    let g:colors_name = expand("<sfile>:t:r")
    command! -nargs=+ CSAHi exe "hi" substitute(substitute(<q-args>, "undercurl", "underline", "g"), "guisp\\S\\+", "", "g")
else
    let g:colors_name = expand("<sfile>:t:r")
    command! -nargs=+ CSAHi exe "hi" <q-args>
endif

if 0
elseif has("gui_running") || (&t_Co == 256 && (&term ==# "xterm" || &term =~# "^screen") && exists("g:CSApprox_konsole") && g:CSApprox_konsole) || &term =~? "^konsole"
    CSAHi Normal term=NONE cterm=NONE ctermbg=234 ctermfg=246 gui=NONE guibg=#202020 guifg=#969696
    CSAHi cUserCont term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi cBitField term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi cppMinMax term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi PreProc term=underline cterm=NONE ctermbg=bg ctermfg=32 gui=NONE guibg=bg guifg=#009acd
    CSAHi Type term=underline cterm=NONE ctermbg=bg ctermfg=252 gui=NONE guibg=bg guifg=#cccccc
    CSAHi Underlined term=underline cterm=underline ctermbg=bg ctermfg=111 gui=underline guibg=bg guifg=#80a0ff
    CSAHi Ignore term=NONE cterm=NONE ctermbg=bg ctermfg=234 gui=NONE guibg=bg guifg=bg
    CSAHi Error term=reverse cterm=underline ctermbg=bg ctermfg=203 gui=underline guibg=bg guifg=#ff3030
    CSAHi Todo term=NONE cterm=bold ctermbg=bg ctermfg=213 gui=bold guibg=bg guifg=#ff88ee
    CSAHi String term=NONE cterm=NONE ctermbg=bg ctermfg=139 gui=NONE guibg=bg guifg=#ab82ab
    CSAHi Boolean term=NONE cterm=NONE ctermbg=bg ctermfg=173 gui=NONE guibg=bg guifg=#e08562
    CSAHi diffNewFile term=NONE cterm=NONE ctermbg=bg ctermfg=226 gui=italic guibg=bg guifg=#ffff00
    CSAHi SpecialKey term=bold cterm=NONE ctermbg=bg ctermfg=241 gui=NONE guibg=bg guifg=#666666
    CSAHi NonText term=bold cterm=NONE ctermbg=234 ctermfg=110 gui=NONE guibg=bg guifg=#9ac0cd
    CSAHi Directory term=bold cterm=NONE ctermbg=234 ctermfg=33 gui=NONE guibg=bg guifg=#1e90ff
    CSAHi ErrorMsg term=NONE cterm=bold ctermbg=bg ctermfg=203 gui=bold guibg=bg guifg=#ff6a6a
    CSAHi IncSearch term=reverse cterm=bold ctermbg=202 ctermfg=231 gui=bold guibg=#ff4500 guifg=#ffffff
    CSAHi Search term=reverse cterm=bold ctermbg=213 ctermfg=16 gui=bold guibg=#ff88ee guifg=#000000
    CSAHi MoreMsg term=bold cterm=bold ctermbg=234 ctermfg=29 gui=bold guibg=bg guifg=#2e8b57
    CSAHi ModeMsg term=bold cterm=bold ctermbg=46 ctermfg=16 gui=bold guibg=#00ff00 guifg=#000000
    CSAHi LineNr term=underline cterm=NONE ctermbg=234 ctermfg=238 gui=NONE guibg=bg guifg=#464646
    CSAHi VimError term=NONE cterm=bold ctermbg=233 ctermfg=203 gui=bold guibg=#101010 guifg=#ff6a6a
    CSAHi qfFileName term=NONE cterm=NONE ctermbg=bg ctermfg=66 gui=italic guibg=bg guifg=#607b8b
    CSAHi qfLineNr term=NONE cterm=bold ctermbg=bg ctermfg=31 gui=bold guibg=bg guifg=#0088aa
    CSAHi qfError term=NONE cterm=bold ctermbg=bg ctermfg=196 gui=bold guibg=bg guifg=#ff0000
    CSAHi pythonDecorator term=NONE cterm=NONE ctermbg=bg ctermfg=104 gui=NONE guibg=bg guifg=#7a86cc
    CSAHi cMulti term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi CursorIM term=NONE cterm=NONE ctermbg=246 ctermfg=234 gui=NONE guibg=fg guifg=bg
    CSAHi diffFile term=NONE cterm=NONE ctermbg=bg ctermfg=214 gui=italic guibg=bg guifg=#ffa500
    CSAHi diffLine term=NONE cterm=NONE ctermbg=bg ctermfg=201 gui=italic guibg=bg guifg=#ff00ff
    CSAHi SpellLocal term=underline cterm=undercurl ctermbg=bg ctermfg=23 gui=undercurl guibg=bg guifg=fg guisp=#007068
    CSAHi Pmenu term=NONE cterm=bold ctermbg=152 ctermfg=21 gui=bold guibg=#c0c8cf guifg=#0000ff
    CSAHi PmenuSel term=NONE cterm=bold ctermbg=21 ctermfg=152 gui=bold guibg=#0000ff guifg=#c0c8cf
    CSAHi PmenuSbar term=NONE cterm=NONE ctermbg=151 ctermfg=231 gui=NONE guibg=#c1cdc1 guifg=#ffffff
    CSAHi PmenuThumb term=NONE cterm=NONE ctermbg=102 ctermfg=231 gui=NONE guibg=#838b83 guifg=#ffffff
    CSAHi TabLine term=underline cterm=underline ctermbg=252 ctermfg=246 gui=underline guibg=#d3d3d3 guifg=fg
    CSAHi TabLineSel term=bold cterm=bold ctermbg=234 ctermfg=246 gui=bold guibg=bg guifg=fg
    CSAHi TabLineFill term=reverse cterm=NONE ctermbg=246 ctermfg=234 gui=reverse guibg=bg guifg=fg
    CSAHi CursorColumn term=reverse cterm=NONE ctermbg=254 gui=NONE guibg=Grey90
    CSAHi CursorLine term=underline cterm=NONE ctermbg=235 gui=NONE guibg=#2a2a2a 
    CSAHi cNumbersCom term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi cCppParen term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi cParen term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi cCppBracket term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi cBracket term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi Function term=NONE cterm=NONE ctermbg=bg ctermfg=83 gui=NONE guibg=bg guifg=#61fd38
    CSAHi Keyword term=NONE cterm=NONE ctermbg=bg ctermfg=252 gui=NONE guibg=bg guifg=#cccccc
    CSAHi Question term=NONE cterm=bold ctermbg=bg ctermfg=231 gui=bold guibg=bg guifg=#fcfcfc
    CSAHi StatusLine term=reverse,bold cterm=NONE ctermbg=248 ctermfg=234 gui=NONE guibg=#aaaaaa guifg=#202020
    CSAHi StatusLineNC term=reverse cterm=NONE ctermbg=59 ctermfg=246 gui=italic guibg=#445566 guifg=#999999
    CSAHi VertSplit term=reverse cterm=NONE ctermbg=59 ctermfg=59 gui=reverse guibg=#445566 guifg=#445566
    CSAHi Title term=bold cterm=bold ctermbg=234 ctermfg=32 gui=bold guibg=bg guifg=#009acd
    CSAHi Visual term=reverse cterm=NONE ctermbg=177 ctermfg=234 gui=NONE guibg=#e177fc guifg=#202020
    CSAHi VisualNOS term=bold,underline cterm=bold,underline ctermbg=bg ctermfg=fg gui=bold,underline guibg=bg guifg=fg
    CSAHi WarningMsg term=NONE cterm=NONE ctermbg=234 ctermfg=208 gui=NONE guibg=bg guifg=#ee9a00
    CSAHi WildMenu term=NONE cterm=NONE ctermbg=116 ctermfg=16 gui=NONE guibg=#87ceeb guifg=#000000
    CSAHi Folded term=NONE cterm=NONE ctermbg=239 ctermfg=66 gui=NONE guibg=#4B4B4B guifg=#68838b
    CSAHi MarkWord3 term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi MarkWord4 term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi MarkWord5 term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi MarkWord6 term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi ColorColumn term=reverse cterm=NONE ctermbg=237 ctermfg=fg gui=NONE guibg=#3A3A3A guifg=fg
    CSAHi Cursor term=NONE cterm=NONE ctermbg=226 ctermfg=234 gui=NONE guibg=#fff000 guifg=bg
    CSAHi lCursor term=NONE cterm=NONE ctermbg=246 ctermfg=234 gui=NONE guibg=fg guifg=bg
    CSAHi MatchParen term=reverse cterm=bold ctermbg=233 ctermfg=226 gui=bold guibg=#101010 guifg=#fff000
    CSAHi Comment term=bold cterm=NONE ctermbg=bg ctermfg=65 gui=italic guibg=bg guifg=#559b70
    CSAHi Constant term=underline cterm=NONE ctermbg=bg ctermfg=173 gui=NONE guibg=bg guifg=#e08562
    CSAHi Special term=bold cterm=NONE ctermbg=bg ctermfg=65 gui=NONE guibg=bg guifg=#559b70
    CSAHi Identifier term=underline cterm=NONE ctermbg=bg ctermfg=221 gui=NONE guibg=bg guifg=#fdde73
    CSAHi Statement term=bold cterm=NONE ctermbg=bg ctermfg=104 gui=NONE guibg=bg guifg=#7a86cc
    CSAHi cNumbers term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi diffOldFile term=NONE cterm=NONE ctermbg=bg ctermfg=170 gui=italic guibg=bg guifg=#da70d6
    CSAHi NONE term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi diffRemoved term=NONE cterm=NONE ctermbg=bg ctermfg=167 gui=NONE guibg=bg guifg=#cd5555
    CSAHi diffChanged term=NONE cterm=NONE ctermbg=bg ctermfg=68 gui=NONE guibg=bg guifg=#4f94cd
    CSAHi diffAdded term=NONE cterm=NONE ctermbg=bg ctermfg=40 gui=NONE guibg=bg guifg=#00cd00
    CSAHi cBlock term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi VimCommentTitle term=NONE cterm=bold ctermbg=234 ctermfg=66 gui=bold,italic guibg=bg guifg=#528b8b
    CSAHi MarkWord1 term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi MarkWord2 term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi FoldColumn term=NONE cterm=bold ctermbg=239 ctermfg=66 gui=bold guibg=#4B4B4B guifg=#68838b
    CSAHi DiffAdd term=bold cterm=NONE ctermbg=71 ctermfg=16 gui=NONE guibg=#3cb371 guifg=#000000
    CSAHi DiffChange term=bold cterm=NONE ctermbg=68 ctermfg=16 gui=NONE guibg=#4f94cd guifg=#000000
    CSAHi DiffDelete term=bold cterm=NONE ctermbg=94 ctermfg=16 gui=NONE guibg=#8b3626 guifg=#000000
    CSAHi DiffText term=reverse cterm=NONE ctermbg=117 ctermfg=16 gui=NONE guibg=#8ee5ee guifg=#000000
    CSAHi SignColumn term=NONE cterm=NONE ctermbg=187 ctermfg=231 gui=NONE guibg=#cdcdb4 guifg=#ffffff
    CSAHi Conceal term=NONE cterm=NONE ctermbg=248 ctermfg=252 gui=NONE guibg=DarkGrey guifg=LightGrey
    CSAHi SpellBad term=reverse cterm=undercurl ctermbg=bg ctermfg=130 gui=undercurl guibg=bg guifg=fg guisp=#c03000
    CSAHi SpellCap term=reverse cterm=undercurl ctermbg=bg ctermfg=25 gui=undercurl guibg=bg guifg=fg guisp=#2060a8
    CSAHi SpellRare term=reverse cterm=undercurl ctermbg=bg ctermfg=133 gui=undercurl guibg=bg guifg=fg guisp=#a030a0
elseif has("gui_running") || (&t_Co == 256 && (&term ==# "xterm" || &term =~# "^screen") && exists("g:CSApprox_eterm") && g:CSApprox_eterm) || &term =~? "^eterm"
    CSAHi Normal term=NONE cterm=NONE ctermbg=234 ctermfg=246 gui=NONE guibg=#202020 guifg=#969696
    CSAHi cUserCont term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi cBitField term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi cppMinMax term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi PreProc term=underline cterm=NONE ctermbg=bg ctermfg=32 gui=NONE guibg=bg guifg=#009acd
    CSAHi Type term=underline cterm=NONE ctermbg=bg ctermfg=252 gui=NONE guibg=bg guifg=#cccccc
    CSAHi Underlined term=underline cterm=underline ctermbg=bg ctermfg=111 gui=underline guibg=bg guifg=#80a0ff
    CSAHi Ignore term=NONE cterm=NONE ctermbg=bg ctermfg=234 gui=NONE guibg=bg guifg=bg
    CSAHi Error term=reverse cterm=underline ctermbg=bg ctermfg=203 gui=underline guibg=bg guifg=#ff3030
    CSAHi Todo term=NONE cterm=bold ctermbg=bg ctermfg=213 gui=bold guibg=bg guifg=#ff88ee
    CSAHi String term=NONE cterm=NONE ctermbg=bg ctermfg=139 gui=NONE guibg=bg guifg=#ab82ab
    CSAHi Boolean term=NONE cterm=NONE ctermbg=bg ctermfg=173 gui=NONE guibg=bg guifg=#e08562
    CSAHi diffNewFile term=NONE cterm=NONE ctermbg=bg ctermfg=226 gui=italic guibg=bg guifg=#ffff00
    CSAHi SpecialKey term=bold cterm=NONE ctermbg=bg ctermfg=241 gui=NONE guibg=bg guifg=#666666
    CSAHi NonText term=bold cterm=NONE ctermbg=234 ctermfg=110 gui=NONE guibg=bg guifg=#9ac0cd
    CSAHi Directory term=bold cterm=NONE ctermbg=234 ctermfg=33 gui=NONE guibg=bg guifg=#1e90ff
    CSAHi ErrorMsg term=NONE cterm=bold ctermbg=bg ctermfg=203 gui=bold guibg=bg guifg=#ff6a6a
    CSAHi IncSearch term=reverse cterm=bold ctermbg=202 ctermfg=231 gui=bold guibg=#ff4500 guifg=#ffffff
    CSAHi Search term=reverse cterm=bold ctermbg=213 ctermfg=16 gui=bold guibg=#ff88ee guifg=#000000
    CSAHi MoreMsg term=bold cterm=bold ctermbg=234 ctermfg=29 gui=bold guibg=bg guifg=#2e8b57
    CSAHi ModeMsg term=bold cterm=bold ctermbg=46 ctermfg=16 gui=bold guibg=#00ff00 guifg=#000000
    CSAHi LineNr term=underline cterm=NONE ctermbg=234 ctermfg=238 gui=NONE guibg=bg guifg=#464646
    CSAHi VimError term=NONE cterm=bold ctermbg=233 ctermfg=203 gui=bold guibg=#101010 guifg=#ff6a6a
    CSAHi qfFileName term=NONE cterm=NONE ctermbg=bg ctermfg=66 gui=italic guibg=bg guifg=#607b8b
    CSAHi qfLineNr term=NONE cterm=bold ctermbg=bg ctermfg=31 gui=bold guibg=bg guifg=#0088aa
    CSAHi qfError term=NONE cterm=bold ctermbg=bg ctermfg=196 gui=bold guibg=bg guifg=#ff0000
    CSAHi pythonDecorator term=NONE cterm=NONE ctermbg=bg ctermfg=104 gui=NONE guibg=bg guifg=#7a86cc
    CSAHi cMulti term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi CursorIM term=NONE cterm=NONE ctermbg=246 ctermfg=234 gui=NONE guibg=fg guifg=bg
    CSAHi diffFile term=NONE cterm=NONE ctermbg=bg ctermfg=214 gui=italic guibg=bg guifg=#ffa500
    CSAHi diffLine term=NONE cterm=NONE ctermbg=bg ctermfg=201 gui=italic guibg=bg guifg=#ff00ff
    CSAHi SpellLocal term=underline cterm=undercurl ctermbg=bg ctermfg=23 gui=undercurl guibg=bg guifg=fg guisp=#007068
    CSAHi Pmenu term=NONE cterm=bold ctermbg=152 ctermfg=21 gui=bold guibg=#c0c8cf guifg=#0000ff
    CSAHi PmenuSel term=NONE cterm=bold ctermbg=21 ctermfg=152 gui=bold guibg=#0000ff guifg=#c0c8cf
    CSAHi PmenuSbar term=NONE cterm=NONE ctermbg=151 ctermfg=231 gui=NONE guibg=#c1cdc1 guifg=#ffffff
    CSAHi PmenuThumb term=NONE cterm=NONE ctermbg=102 ctermfg=231 gui=NONE guibg=#838b83 guifg=#ffffff
    CSAHi TabLine term=underline cterm=underline ctermbg=252 ctermfg=246 gui=underline guibg=#d3d3d3 guifg=fg
    CSAHi TabLineSel term=bold cterm=bold ctermbg=234 ctermfg=246 gui=bold guibg=bg guifg=fg
    CSAHi TabLineFill term=reverse cterm=NONE ctermbg=246 ctermfg=234 gui=reverse guibg=bg guifg=fg
    CSAHi CursorColumn term=reverse cterm=NONE ctermbg=254 gui=NONE guibg=Grey90
    CSAHi CursorLine term=underline cterm=NONE ctermbg=235 gui=NONE guibg=#2a2a2a
    CSAHi cNumbersCom term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi cCppParen term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi cParen term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi cCppBracket term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi cBracket term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi Function term=NONE cterm=NONE ctermbg=bg ctermfg=83 gui=NONE guibg=bg guifg=#61fd38
    CSAHi Keyword term=NONE cterm=NONE ctermbg=bg ctermfg=252 gui=NONE guibg=bg guifg=#cccccc
    CSAHi Question term=NONE cterm=bold ctermbg=bg ctermfg=231 gui=bold guibg=bg guifg=#fcfcfc
    CSAHi StatusLine term=reverse,bold cterm=NONE ctermbg=248 ctermfg=234 gui=NONE guibg=#aaaaaa guifg=#202020
    CSAHi StatusLineNC term=reverse cterm=NONE ctermbg=59 ctermfg=246 gui=italic guibg=#445566 guifg=#999999
    CSAHi VertSplit term=reverse cterm=NONE ctermbg=59 ctermfg=59 gui=reverse guibg=#445566 guifg=#445566
    CSAHi Title term=bold cterm=bold ctermbg=234 ctermfg=32 gui=bold guibg=bg guifg=#009acd
    CSAHi Visual term=reverse cterm=NONE ctermbg=177 ctermfg=234 gui=NONE guibg=#e177fc guifg=#202020
    CSAHi VisualNOS term=bold,underline cterm=bold,underline ctermbg=bg ctermfg=fg gui=bold,underline guibg=bg guifg=fg
    CSAHi WarningMsg term=NONE cterm=NONE ctermbg=234 ctermfg=208 gui=NONE guibg=bg guifg=#ee9a00
    CSAHi WildMenu term=NONE cterm=NONE ctermbg=116 ctermfg=16 gui=NONE guibg=#87ceeb guifg=#000000
    CSAHi Folded term=NONE cterm=NONE ctermbg=239 ctermfg=66 gui=NONE guibg=#4B4B4B guifg=#68838b
    CSAHi MarkWord3 term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi MarkWord4 term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi MarkWord5 term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi MarkWord6 term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi ColorColumn term=reverse cterm=NONE ctermbg=237 ctermfg=fg gui=NONE guibg=#3A3A3A guifg=fg
    CSAHi Cursor term=NONE cterm=NONE ctermbg=226 ctermfg=234 gui=NONE guibg=#fff000 guifg=bg
    CSAHi lCursor term=NONE cterm=NONE ctermbg=246 ctermfg=234 gui=NONE guibg=fg guifg=bg
    CSAHi MatchParen term=reverse cterm=bold ctermbg=233 ctermfg=226 gui=bold guibg=#101010 guifg=#fff000
    CSAHi Comment term=bold cterm=NONE ctermbg=bg ctermfg=65 gui=italic guibg=bg guifg=#559b70
    CSAHi Constant term=underline cterm=NONE ctermbg=bg ctermfg=173 gui=NONE guibg=bg guifg=#e08562
    CSAHi Special term=bold cterm=NONE ctermbg=bg ctermfg=65 gui=NONE guibg=bg guifg=#559b70
    CSAHi Identifier term=underline cterm=NONE ctermbg=bg ctermfg=221 gui=NONE guibg=bg guifg=#fdde73
    CSAHi Statement term=bold cterm=NONE ctermbg=bg ctermfg=104 gui=NONE guibg=bg guifg=#7a86cc
    CSAHi cNumbers term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi diffOldFile term=NONE cterm=NONE ctermbg=bg ctermfg=170 gui=italic guibg=bg guifg=#da70d6
    CSAHi NONE term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi diffRemoved term=NONE cterm=NONE ctermbg=bg ctermfg=167 gui=NONE guibg=bg guifg=#cd5555
    CSAHi diffChanged term=NONE cterm=NONE ctermbg=bg ctermfg=68 gui=NONE guibg=bg guifg=#4f94cd
    CSAHi diffAdded term=NONE cterm=NONE ctermbg=bg ctermfg=40 gui=NONE guibg=bg guifg=#00cd00
    CSAHi cBlock term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi VimCommentTitle term=NONE cterm=bold ctermbg=234 ctermfg=66 gui=bold,italic guibg=bg guifg=#528b8b
    CSAHi MarkWord1 term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi MarkWord2 term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi FoldColumn term=NONE cterm=bold ctermbg=239 ctermfg=66 gui=bold guibg=#4B4B4B guifg=#68838b
    CSAHi DiffAdd term=bold cterm=NONE ctermbg=71 ctermfg=16 gui=NONE guibg=#3cb371 guifg=#000000
    CSAHi DiffChange term=bold cterm=NONE ctermbg=68 ctermfg=16 gui=NONE guibg=#4f94cd guifg=#000000
    CSAHi DiffDelete term=bold cterm=NONE ctermbg=94 ctermfg=16 gui=NONE guibg=#8b3626 guifg=#000000
    CSAHi DiffText term=reverse cterm=NONE ctermbg=117 ctermfg=16 gui=NONE guibg=#8ee5ee guifg=#000000
    CSAHi SignColumn term=NONE cterm=NONE ctermbg=187 ctermfg=231 gui=NONE guibg=#cdcdb4 guifg=#ffffff
    CSAHi Conceal term=NONE cterm=NONE ctermbg=248 ctermfg=252 gui=NONE guibg=DarkGrey guifg=LightGrey
    CSAHi SpellBad term=reverse cterm=undercurl ctermbg=bg ctermfg=130 gui=undercurl guibg=bg guifg=fg guisp=#c03000
    CSAHi SpellCap term=reverse cterm=undercurl ctermbg=bg ctermfg=25 gui=undercurl guibg=bg guifg=fg guisp=#2060a8
    CSAHi SpellRare term=reverse cterm=undercurl ctermbg=bg ctermfg=133 gui=undercurl guibg=bg guifg=fg guisp=#a030a0
elseif has("gui_running") || &t_Co == 256
    CSAHi Normal term=NONE cterm=NONE ctermbg=234 ctermfg=246 gui=NONE guibg=#202020 guifg=#969696
    CSAHi cUserCont term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi cBitField term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi cppMinMax term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi PreProc term=underline cterm=NONE ctermbg=bg ctermfg=32 gui=NONE guibg=bg guifg=#009acd
    CSAHi Type term=underline cterm=NONE ctermbg=bg ctermfg=252 gui=NONE guibg=bg guifg=#cccccc
    CSAHi Underlined term=underline cterm=underline ctermbg=bg ctermfg=111 gui=underline guibg=bg guifg=#80a0ff
    CSAHi Ignore term=NONE cterm=NONE ctermbg=bg ctermfg=234 gui=NONE guibg=bg guifg=bg
    CSAHi Error term=reverse cterm=underline ctermbg=bg ctermfg=203 gui=underline guibg=bg guifg=#ff3030
    CSAHi Todo term=NONE cterm=bold ctermbg=bg ctermfg=213 gui=bold guibg=bg guifg=#ff88ee
    CSAHi String term=NONE cterm=NONE ctermbg=bg ctermfg=139 gui=NONE guibg=bg guifg=#ab82ab
    CSAHi Boolean term=NONE cterm=NONE ctermbg=bg ctermfg=173 gui=NONE guibg=bg guifg=#e08562
    CSAHi diffNewFile term=NONE cterm=NONE ctermbg=bg ctermfg=226 gui=italic guibg=bg guifg=#ffff00
    CSAHi SpecialKey term=bold cterm=NONE ctermbg=bg ctermfg=241 gui=NONE guibg=bg guifg=#666666
    CSAHi NonText term=bold cterm=NONE ctermbg=234 ctermfg=110 gui=NONE guibg=bg guifg=#9ac0cd
    CSAHi Directory term=bold cterm=NONE ctermbg=234 ctermfg=33 gui=NONE guibg=bg guifg=#1e90ff
    CSAHi ErrorMsg term=NONE cterm=bold ctermbg=bg ctermfg=203 gui=bold guibg=bg guifg=#ff6a6a
    CSAHi IncSearch term=reverse cterm=bold ctermbg=202 ctermfg=231 gui=bold guibg=#ff4500 guifg=#ffffff
    CSAHi Search term=reverse cterm=bold ctermbg=213 ctermfg=16 gui=bold guibg=#ff88ee guifg=#000000
    CSAHi MoreMsg term=bold cterm=bold ctermbg=234 ctermfg=29 gui=bold guibg=bg guifg=#2e8b57
    CSAHi ModeMsg term=bold cterm=bold ctermbg=46 ctermfg=16 gui=bold guibg=#00ff00 guifg=#000000
    CSAHi LineNr term=underline cterm=NONE ctermbg=234 ctermfg=238 gui=NONE guibg=bg guifg=#464646
    CSAHi VimError term=NONE cterm=bold ctermbg=233 ctermfg=203 gui=bold guibg=#101010 guifg=#ff6a6a
    CSAHi qfFileName term=NONE cterm=NONE ctermbg=bg ctermfg=66 gui=italic guibg=bg guifg=#607b8b
    CSAHi qfLineNr term=NONE cterm=bold ctermbg=bg ctermfg=31 gui=bold guibg=bg guifg=#0088aa
    CSAHi qfError term=NONE cterm=bold ctermbg=bg ctermfg=196 gui=bold guibg=bg guifg=#ff0000
    CSAHi pythonDecorator term=NONE cterm=NONE ctermbg=bg ctermfg=104 gui=NONE guibg=bg guifg=#7a86cc
    CSAHi cMulti term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi CursorIM term=NONE cterm=NONE ctermbg=246 ctermfg=234 gui=NONE guibg=fg guifg=bg
    CSAHi diffFile term=NONE cterm=NONE ctermbg=bg ctermfg=214 gui=italic guibg=bg guifg=#ffa500
    CSAHi diffLine term=NONE cterm=NONE ctermbg=bg ctermfg=201 gui=italic guibg=bg guifg=#ff00ff
    CSAHi SpellLocal term=underline cterm=undercurl ctermbg=bg ctermfg=23 gui=undercurl guibg=bg guifg=fg guisp=#007068
    CSAHi Pmenu term=NONE cterm=bold ctermbg=152 ctermfg=21 gui=bold guibg=#c0c8cf guifg=#0000ff
    CSAHi PmenuSel term=NONE cterm=bold ctermbg=21 ctermfg=152 gui=bold guibg=#0000ff guifg=#c0c8cf
    CSAHi PmenuSbar term=NONE cterm=NONE ctermbg=151 ctermfg=231 gui=NONE guibg=#c1cdc1 guifg=#ffffff
    CSAHi PmenuThumb term=NONE cterm=NONE ctermbg=102 ctermfg=231 gui=NONE guibg=#838b83 guifg=#ffffff
    CSAHi TabLine term=underline cterm=underline ctermbg=252 ctermfg=246 gui=underline guibg=#d3d3d3 guifg=fg
    CSAHi TabLineSel term=bold cterm=bold ctermbg=234 ctermfg=246 gui=bold guibg=bg guifg=fg
    CSAHi TabLineFill term=reverse cterm=NONE ctermbg=246 ctermfg=234 gui=reverse guibg=bg guifg=fg
    CSAHi CursorColumn term=reverse cterm=NONE ctermbg=254 gui=NONE guibg=Grey90 
    CSAHi CursorLine term=underline cterm=NONE ctermbg=235 gui=NONE guibg=#2a2a2a
    CSAHi cNumbersCom term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi cCppParen term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi cParen term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi cCppBracket term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi cBracket term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi Function term=NONE cterm=NONE ctermbg=bg ctermfg=83 gui=NONE guibg=bg guifg=#61fd38
    CSAHi Keyword term=NONE cterm=NONE ctermbg=bg ctermfg=252 gui=NONE guibg=bg guifg=#cccccc
    CSAHi Question term=NONE cterm=bold ctermbg=bg ctermfg=231 gui=bold guibg=bg guifg=#fcfcfc
    CSAHi StatusLine term=reverse,bold cterm=NONE ctermbg=248 ctermfg=234 gui=NONE guibg=#aaaaaa guifg=#202020
    CSAHi StatusLineNC term=reverse cterm=NONE ctermbg=59 ctermfg=246 gui=italic guibg=#445566 guifg=#999999
    CSAHi VertSplit term=reverse cterm=NONE ctermbg=59 ctermfg=59 gui=reverse guibg=#445566 guifg=#445566
    CSAHi Title term=bold cterm=bold ctermbg=234 ctermfg=32 gui=bold guibg=bg guifg=#009acd
    CSAHi Visual term=reverse cterm=NONE ctermbg=177 ctermfg=234 gui=NONE guibg=#e177fc guifg=#202020
    CSAHi VisualNOS term=bold,underline cterm=bold,underline ctermbg=bg ctermfg=fg gui=bold,underline guibg=bg guifg=fg
    CSAHi WarningMsg term=NONE cterm=NONE ctermbg=234 ctermfg=208 gui=NONE guibg=bg guifg=#ee9a00
    CSAHi WildMenu term=NONE cterm=NONE ctermbg=116 ctermfg=16 gui=NONE guibg=#87ceeb guifg=#000000
    CSAHi Folded term=NONE cterm=NONE ctermbg=239 ctermfg=66 gui=NONE guibg=#4B4B4B guifg=#68838b
    CSAHi MarkWord3 term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi MarkWord4 term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi MarkWord5 term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi MarkWord6 term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi ColorColumn term=reverse cterm=NONE ctermbg=237 ctermfg=fg gui=NONE guibg=#3A3A3A guifg=fg
    CSAHi Cursor term=NONE cterm=NONE ctermbg=226 ctermfg=234 gui=NONE guibg=#fff000 guifg=bg
    CSAHi lCursor term=NONE cterm=NONE ctermbg=246 ctermfg=234 gui=NONE guibg=fg guifg=bg
    CSAHi MatchParen term=reverse cterm=bold ctermbg=233 ctermfg=226 gui=bold guibg=#101010 guifg=#fff000
    CSAHi Comment term=bold cterm=NONE ctermbg=bg ctermfg=65 gui=italic guibg=bg guifg=#559b70
    CSAHi Constant term=underline cterm=NONE ctermbg=bg ctermfg=173 gui=NONE guibg=bg guifg=#e08562
    CSAHi Special term=bold cterm=NONE ctermbg=bg ctermfg=65 gui=NONE guibg=bg guifg=#559b70
    CSAHi Identifier term=underline cterm=NONE ctermbg=bg ctermfg=221 gui=NONE guibg=bg guifg=#fdde73
    CSAHi Statement term=bold cterm=NONE ctermbg=bg ctermfg=104 gui=NONE guibg=bg guifg=#7a86cc
    CSAHi cNumbers term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi diffOldFile term=NONE cterm=NONE ctermbg=bg ctermfg=170 gui=italic guibg=bg guifg=#da70d6
    CSAHi NONE term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi diffRemoved term=NONE cterm=NONE ctermbg=bg ctermfg=167 gui=NONE guibg=bg guifg=#cd5555
    CSAHi diffChanged term=NONE cterm=NONE ctermbg=bg ctermfg=68 gui=NONE guibg=bg guifg=#4f94cd
    CSAHi diffAdded term=NONE cterm=NONE ctermbg=bg ctermfg=40 gui=NONE guibg=bg guifg=#00cd00
    CSAHi cBlock term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi VimCommentTitle term=NONE cterm=bold ctermbg=234 ctermfg=66 gui=bold,italic guibg=bg guifg=#528b8b
    CSAHi MarkWord1 term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi MarkWord2 term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi FoldColumn term=NONE cterm=bold ctermbg=239 ctermfg=66 gui=bold guibg=#4B4B4B guifg=#68838b
    CSAHi DiffAdd term=bold cterm=NONE ctermbg=71 ctermfg=16 gui=NONE guibg=#3cb371 guifg=#000000
    CSAHi DiffChange term=bold cterm=NONE ctermbg=68 ctermfg=16 gui=NONE guibg=#4f94cd guifg=#000000
    CSAHi DiffDelete term=bold cterm=NONE ctermbg=94 ctermfg=16 gui=NONE guibg=#8b3626 guifg=#000000
    CSAHi DiffText term=reverse cterm=NONE ctermbg=117 ctermfg=16 gui=NONE guibg=#8ee5ee guifg=#000000
    CSAHi SignColumn term=NONE cterm=NONE ctermbg=187 ctermfg=231 gui=NONE guibg=#cdcdb4 guifg=#ffffff
    CSAHi Conceal term=NONE cterm=NONE ctermbg=248 ctermfg=252 gui=NONE guibg=DarkGrey guifg=LightGrey
    CSAHi SpellBad term=reverse cterm=undercurl ctermbg=bg ctermfg=130 gui=undercurl guibg=bg guifg=fg guisp=#c03000
    CSAHi SpellCap term=reverse cterm=undercurl ctermbg=bg ctermfg=25 gui=undercurl guibg=bg guifg=fg guisp=#2060a8
    CSAHi SpellRare term=reverse cterm=undercurl ctermbg=bg ctermfg=133 gui=undercurl guibg=bg guifg=fg guisp=#a030a0
elseif has("gui_running") || &t_Co == 88
    CSAHi Normal term=NONE cterm=NONE ctermbg=80 ctermfg=83 gui=NONE guibg=#202020 guifg=#969696
    CSAHi cUserCont term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi cBitField term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi cppMinMax term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi PreProc term=underline cterm=NONE ctermbg=bg ctermfg=22 gui=NONE guibg=bg guifg=#009acd
    CSAHi Type term=underline cterm=NONE ctermbg=bg ctermfg=58 gui=NONE guibg=bg guifg=#cccccc
    CSAHi Underlined term=underline cterm=underline ctermbg=bg ctermfg=39 gui=underline guibg=bg guifg=#80a0ff
    CSAHi Ignore term=NONE cterm=NONE ctermbg=bg ctermfg=80 gui=NONE guibg=bg guifg=bg
    CSAHi Error term=reverse cterm=underline ctermbg=bg ctermfg=64 gui=underline guibg=bg guifg=#ff3030
    CSAHi Todo term=NONE cterm=bold ctermbg=bg ctermfg=71 gui=bold guibg=bg guifg=#ff88ee
    CSAHi String term=NONE cterm=NONE ctermbg=bg ctermfg=37 gui=NONE guibg=bg guifg=#ab82ab
    CSAHi Boolean term=NONE cterm=NONE ctermbg=bg ctermfg=53 gui=NONE guibg=bg guifg=#e08562
    CSAHi diffNewFile term=NONE cterm=NONE ctermbg=bg ctermfg=76 gui=italic guibg=bg guifg=#ffff00
    CSAHi SpecialKey term=bold cterm=NONE ctermbg=bg ctermfg=81 gui=NONE guibg=bg guifg=#666666
    CSAHi NonText term=bold cterm=NONE ctermbg=80 ctermfg=42 gui=NONE guibg=bg guifg=#9ac0cd
    CSAHi Directory term=bold cterm=NONE ctermbg=80 ctermfg=23 gui=NONE guibg=bg guifg=#1e90ff
    CSAHi ErrorMsg term=NONE cterm=bold ctermbg=bg ctermfg=69 gui=bold guibg=bg guifg=#ff6a6a
    CSAHi IncSearch term=reverse cterm=bold ctermbg=64 ctermfg=79 gui=bold guibg=#ff4500 guifg=#ffffff
    CSAHi Search term=reverse cterm=bold ctermbg=71 ctermfg=16 gui=bold guibg=#ff88ee guifg=#000000
    CSAHi MoreMsg term=bold cterm=bold ctermbg=80 ctermfg=21 gui=bold guibg=bg guifg=#2e8b57
    CSAHi ModeMsg term=bold cterm=bold ctermbg=28 ctermfg=16 gui=bold guibg=#00ff00 guifg=#000000
    CSAHi LineNr term=underline cterm=NONE ctermbg=80 ctermfg=81 gui=NONE guibg=bg guifg=#464646
    CSAHi VimError term=NONE cterm=bold ctermbg=16 ctermfg=69 gui=bold guibg=#101010 guifg=#ff6a6a
    CSAHi qfFileName term=NONE cterm=NONE ctermbg=bg ctermfg=37 gui=italic guibg=bg guifg=#607b8b
    CSAHi qfLineNr term=NONE cterm=bold ctermbg=bg ctermfg=21 gui=bold guibg=bg guifg=#0088aa
    CSAHi qfError term=NONE cterm=bold ctermbg=bg ctermfg=64 gui=bold guibg=bg guifg=#ff0000
    CSAHi pythonDecorator term=NONE cterm=NONE ctermbg=bg ctermfg=38 gui=NONE guibg=bg guifg=#7a86cc
    CSAHi cMulti term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi CursorIM term=NONE cterm=NONE ctermbg=83 ctermfg=80 gui=NONE guibg=fg guifg=bg
    CSAHi diffFile term=NONE cterm=NONE ctermbg=bg ctermfg=68 gui=italic guibg=bg guifg=#ffa500
    CSAHi diffLine term=NONE cterm=NONE ctermbg=bg ctermfg=67 gui=italic guibg=bg guifg=#ff00ff
    CSAHi SpellLocal term=underline cterm=undercurl ctermbg=bg ctermfg=21 gui=undercurl guibg=bg guifg=fg guisp=#007068
    CSAHi Pmenu term=NONE cterm=bold ctermbg=58 ctermfg=19 gui=bold guibg=#c0c8cf guifg=#0000ff
    CSAHi PmenuSel term=NONE cterm=bold ctermbg=19 ctermfg=58 gui=bold guibg=#0000ff guifg=#c0c8cf
    CSAHi PmenuSbar term=NONE cterm=NONE ctermbg=58 ctermfg=79 gui=NONE guibg=#c1cdc1 guifg=#ffffff
    CSAHi PmenuThumb term=NONE cterm=NONE ctermbg=83 ctermfg=79 gui=NONE guibg=#838b83 guifg=#ffffff
    CSAHi TabLine term=underline cterm=underline ctermbg=86 ctermfg=83 gui=underline guibg=#d3d3d3 guifg=fg
    CSAHi TabLineSel term=bold cterm=bold ctermbg=80 ctermfg=83 gui=bold guibg=bg guifg=fg
    CSAHi TabLineFill term=reverse cterm=NONE ctermbg=83 ctermfg=80 gui=reverse guibg=bg guifg=fg
    CSAHi CursorColumn term=reverse cterm=NONE ctermbg=87 gui=NONE guibg=Grey90 
    CSAHi CursorLine term=underline cterm=NONE ctermbg=80 gui=NONE guibg=#2a2a2a
    CSAHi cNumbersCom term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi cCppParen term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi cParen term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi cCppBracket term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi cBracket term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi Function term=NONE cterm=NONE ctermbg=bg ctermfg=44 gui=NONE guibg=bg guifg=#61fd38
    CSAHi Keyword term=NONE cterm=NONE ctermbg=bg ctermfg=58 gui=NONE guibg=bg guifg=#cccccc
    CSAHi Question term=NONE cterm=bold ctermbg=bg ctermfg=79 gui=bold guibg=bg guifg=#fcfcfc
    CSAHi StatusLine term=reverse,bold cterm=NONE ctermbg=84 ctermfg=80 gui=NONE guibg=#aaaaaa guifg=#202020
    CSAHi StatusLineNC term=reverse cterm=NONE ctermbg=21 ctermfg=84 gui=italic guibg=#445566 guifg=#999999
    CSAHi VertSplit term=reverse cterm=NONE ctermbg=21 ctermfg=21 gui=reverse guibg=#445566 guifg=#445566
    CSAHi Title term=bold cterm=bold ctermbg=80 ctermfg=22 gui=bold guibg=bg guifg=#009acd
    CSAHi Visual term=reverse cterm=NONE ctermbg=55 ctermfg=80 gui=NONE guibg=#e177fc guifg=#202020
    CSAHi VisualNOS term=bold,underline cterm=bold,underline ctermbg=bg ctermfg=fg gui=bold,underline guibg=bg guifg=fg
    CSAHi WarningMsg term=NONE cterm=NONE ctermbg=80 ctermfg=68 gui=NONE guibg=bg guifg=#ee9a00
    CSAHi WildMenu term=NONE cterm=NONE ctermbg=43 ctermfg=16 gui=NONE guibg=#87ceeb guifg=#000000
    CSAHi Folded term=NONE cterm=NONE ctermbg=81 ctermfg=37 gui=NONE guibg=#4B4B4B guifg=#68838b
    CSAHi MarkWord3 term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi MarkWord4 term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi MarkWord5 term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi MarkWord6 term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi ColorColumn term=reverse cterm=NONE ctermbg=80 ctermfg=fg gui=NONE guibg=#3A3A3A guifg=fg
    CSAHi Cursor term=NONE cterm=NONE ctermbg=76 ctermfg=80 gui=NONE guibg=#fff000 guifg=bg
    CSAHi lCursor term=NONE cterm=NONE ctermbg=83 ctermfg=80 gui=NONE guibg=fg guifg=bg
    CSAHi MatchParen term=reverse cterm=bold ctermbg=16 ctermfg=76 gui=bold guibg=#101010 guifg=#fff000
    CSAHi Comment term=bold cterm=NONE ctermbg=bg ctermfg=37 gui=italic guibg=bg guifg=#559b70
    CSAHi Constant term=underline cterm=NONE ctermbg=bg ctermfg=53 gui=NONE guibg=bg guifg=#e08562
    CSAHi Special term=bold cterm=NONE ctermbg=bg ctermfg=37 gui=NONE guibg=bg guifg=#559b70
    CSAHi Identifier term=underline cterm=NONE ctermbg=bg ctermfg=73 gui=NONE guibg=bg guifg=#fdde73
    CSAHi Statement term=bold cterm=NONE ctermbg=bg ctermfg=38 gui=NONE guibg=bg guifg=#7a86cc
    CSAHi cNumbers term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi diffOldFile term=NONE cterm=NONE ctermbg=bg ctermfg=54 gui=italic guibg=bg guifg=#da70d6
    CSAHi NONE term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi diffRemoved term=NONE cterm=NONE ctermbg=bg ctermfg=53 gui=NONE guibg=bg guifg=#cd5555
    CSAHi diffChanged term=NONE cterm=NONE ctermbg=bg ctermfg=38 gui=NONE guibg=bg guifg=#4f94cd
    CSAHi diffAdded term=NONE cterm=NONE ctermbg=bg ctermfg=24 gui=NONE guibg=bg guifg=#00cd00
    CSAHi cBlock term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi VimCommentTitle term=NONE cterm=bold ctermbg=80 ctermfg=37 gui=bold,italic guibg=bg guifg=#528b8b
    CSAHi MarkWord1 term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi MarkWord2 term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi FoldColumn term=NONE cterm=bold ctermbg=81 ctermfg=37 gui=bold guibg=#4B4B4B guifg=#68838b
    CSAHi DiffAdd term=bold cterm=NONE ctermbg=25 ctermfg=16 gui=NONE guibg=#3cb371 guifg=#000000
    CSAHi DiffChange term=bold cterm=NONE ctermbg=38 ctermfg=16 gui=NONE guibg=#4f94cd guifg=#000000
    CSAHi DiffDelete term=bold cterm=NONE ctermbg=32 ctermfg=16 gui=NONE guibg=#8b3626 guifg=#000000
    CSAHi DiffText term=reverse cterm=NONE ctermbg=43 ctermfg=16 gui=NONE guibg=#8ee5ee guifg=#000000
    CSAHi SignColumn term=NONE cterm=NONE ctermbg=58 ctermfg=79 gui=NONE guibg=#cdcdb4 guifg=#ffffff
    CSAHi Conceal term=NONE cterm=NONE ctermbg=84 ctermfg=86 gui=NONE guibg=DarkGrey guifg=LightGrey
    CSAHi SpellBad term=reverse cterm=undercurl ctermbg=bg ctermfg=48 gui=undercurl guibg=bg guifg=fg guisp=#c03000
    CSAHi SpellCap term=reverse cterm=undercurl ctermbg=bg ctermfg=21 gui=undercurl guibg=bg guifg=fg guisp=#2060a8
    CSAHi SpellRare term=reverse cterm=undercurl ctermbg=bg ctermfg=33 gui=undercurl guibg=bg guifg=fg guisp=#a030a0
endif

if 1
    delcommand CSAHi
endif
