""" add directories to the runtime path
call pathogen#infect()

"""" {{{ STANDARD SETTINGS
""""

set nocompatible
language messages en_GB

set incsearch
set tags+=../tags
set tags+=../../tags
"set tags+=../../../tags
"set tags+=../../../../tags
"set tags+=../../../../../tags
"set tags+=../../../../../../tags
"set tags+=../../../../../../../tags
"set tags+=../../../../../../../../tags
"set tags+=../../../../../../../../../tags
" this should be enough...

if filereadable("$VIMRUNTIME/doc/help.txt.gz")
    set helpfile=$VIMRUNTIME/doc/help.txt.gz
endif

"set path+=/usr/local/include/player-3.0/
set path+=/usr/lib/gcc/i686-pc-linux-gnu/4.4.4/include/g++-v4/

"au BufNewFile,BufRead *.sdf setf stqldefs

set nobackup

set ls=2
set showcmd
set mouse=a
set backspace=indent,eol,start

set scrolloff=5
set sidescrolloff=3
set sidescroll=1
set ignorecase
set smartcase
"set nu
"set foldmethod=indent
"set foldlevelstart=2
set nofoldenable
set wildmenu
set hidden
set confirm
set completeopt=menuone,longest
set previewheight=20
set updatetime=1000
set matchpairs+=<:>
set matchpairs+==:;

set cscopequickfix=s-,c-,d-,i-,t-,e-

set switchbuf=useopen,usetab

if has('gui_running') && !has('win32')
	set encoding=utf8
endif

if match($TERM, "screen") != -1
    map OH <Home>
    map OF <End>
    map! OH <Home>
    map! OF <End>
endif

"if $TERM == tmux:
"   let &t_SI="\<Esc>Ptmux;\<Esc>\<Esc>]12;orange\x7\<Esc>\\"
"   let &t_EI="\<Esc>Ptmux;\<Esc>\<Esc>]12;green\x7\<Esc>\\"

imap <C-@> <C-Space>

inoremap <C-Space> <C-X><C-U>

set lcs=tab:\|\ ,trail:x

filetype plugin indent on
syntax on
set hlsearch

"set background=light
set background=dark

if has("win32")
"set guifont=Consolas:h10:cEASTEUROPE
set gfn=Terminus:h12:cEASTEUROPE
map <C-S-F11> :call libcallnr("gvimfullscreen.dll", "ToggleFullScreen", 0)<CR>
else
set guifont=Consolas\ 9
set guifont=Aerial\ Mono\ 9
endif

set title
set guioptions=agirt
set t_Co=256

"""" }}}
"""" {{{ AUTOCOMMANDS, FUNCTIONS AND HIGHLIGHTING
if v:progname =~! "vi" && !exists(":DiffOrig")
  command DiffOrig vert new | set bt=nofile | r # | 0d_ | diffthis
		  \ | wincmd p | diffthis
endif

augroup vimrcEx
    au!

    " For all text files set 'textwidth' to 78 characters.
    autocmd FileType text setlocal textwidth=78

    " When editing a file, always jump to the last known cursor position.
    " Don't do it when the position is invalid or when inside an event handler
    " (happens when dropping a file on gvim).
    " Also don't do it when the mark is in the first line, that is the default
    " position when opening a file.
    autocmd BufReadPost *
                \ if line("'\"") > 1 && line("'\"") <= line("$") |
                \   exe "normal! g`\"" |
                \ endif



    au FileType cpp,c setl cursorline nu
    au FileType cpp,c :HighlightTags

    au BufEnter *.cpp,*.c,*.C let b:fswitchdst = 'h,hpp,hh,H,hxx'
    au BufEnter *.hpp,*.h,*.hh,*.hxx let b:fswitchdst = 'cpp,cxx,c,cc'

    au FileType text setlocal textwidth=78

    " au InsertLeave,InsertEnter * setl cursorline!
augroup END


command! -nargs=+ -complete=file Cpplint lexpr system("cpplint.py --filter=-whitespace/tab,-whitespace/braces " . <q-args>)

" {{{ foldtext settings
set foldtext=GenFoldText()
au FileType c,cpp,java set foldmethod=syntax
au FileType c,cpp,java set foldtext=CFoldText()

" A nicer foldtext function for C/CPP/JAVA
" SRC: http://vim.wikia.com/wiki/Customize_text_for_closed_folds
function! CFoldText() " {{{
	let line = getline(v:foldstart)
	if match( line, '^[ \t]*\(\/\*\|\/\/\)[*/\\]*[ \t]*$' ) == 0
		let initial = substitute( line, '^\([ \t]\)*\(\/\*\|\/\/\)\(.*\)', '\1\2', '' )
		let linenum = v:foldstart + 1
		while linenum < v:foldend
			let line = getline( linenum )
			let comment_content = substitute( line, '^\([ \t\/\*]*\)\(.*\)$', '\2', 'g' )
			if comment_content != ''
				break
			endif
			let linenum = linenum + 1
		endwhile
		let sub = initial . ' ' . comment_content
	else
		let sub = line
		let startbrace = substitute( line, '^.*{[ \t]*$', '{', 'g')
		if startbrace == '{'
			let line = getline(v:foldend)
			let endbrace = substitute( line, '^[ \t]*}\(.*\)$', '}', 'g')
			if endbrace == '}'
				let sub = sub.substitute( line, '^[ \t]*}\(.*\)$', '...}\1', 'g')
			endif
		endif
	endif
	let n = v:foldend - v:foldstart + 1
	let info = " " . n . " lines"

	let sub = substitute(sub, '\t', repeat(' ', getwinvar(8, '&tabstop')), 'g')
	let sub = sub . "                                                                                                                  "
	let num_w = getwinvar( 0, '&number' ) * getwinvar( 0, '&numberwidth' )
	let fold_w = getwinvar( 0, '&foldcolumn' )
	let sub = strpart( sub, 0, winwidth(0) - strlen( info ) - num_w - fold_w - 1 )
	return sub . info
endfunction " }}}


" SRC: http://vim.wikia.com/wiki/Customize_text_for_closed_folds
function! GenFoldText() " {{{
	" for now, just don't try if version isn't 7 or higher
	if v:version < 701
		return foldtext()
	endif
	" clear fold from fillchars to set it up the way we want later
	let &l:fillchars = substitute(&l:fillchars,',\?fold:.','','gi')
	let l:numwidth = (v:version < 701 ? 8 : &numberwidth)
	if &fdm=='diff'
		let l:linetext=''
		let l:foldtext='---------- '.(v:foldend-v:foldstart+1).' lines the same ----------'
		let l:align = winwidth(0)-&foldcolumn-(&nu ? Max(strlen(line('$'))+1, l:numwidth) : 0)
		let l:align = (l:align / 2) + (strlen(l:foldtext)/2)
		" note trailing space on next line
		setlocal fillchars+=fold:\ 
	elseif !exists('b:foldpat') || b:foldpat==0
		let l:foldtext = ' '.(v:foldend-v:foldstart).' lines folded'.v:folddashes.'|'
		let l:endofline = (&textwidth>0 ? &textwidth : 80)
		let l:linetext = strpart(getline(v:foldstart),0,l:endofline-strlen(l:foldtext))
		let l:align = l:endofline-strlen(l:linetext)
		setlocal fillchars+=fold:-
	elseif b:foldpat==1
		let l:align = winwidth(0)-&foldcolumn-(&nu ? Max(strlen(line('$'))+1, l:numwidth) : 0)
		let l:foldtext = ' '.v:folddashes
		let l:linetext = substitute(getline(v:foldstart),'\s\+$','','')
		let l:linetext .= ' ---'.(v:foldend-v:foldstart-1).' lines--- '
		let l:linetext .= substitute(getline(v:foldend),'^\s\+','','')
		let l:linetext = strpart(l:linetext,0,l:align-strlen(l:foldtext))
		let l:align -= strlen(l:linetext)
		setlocal fillchars+=fold:-
	endif
	return printf('%s%*s', l:linetext, l:align, l:foldtext)
endfunction " }}}
"}}}

" {{{ statusline highlights
" this group needs to be declared before colorscheme setting
augroup Statusline
  au! Statusline

  au WinEnter,BufEnter * call <SID>SetStatusline("active")
  au WinLeave,BufLeave,BufNew,BufRead,BufNewFile * call <SID>SetStatusline("")

  au ColorScheme * call <SID>SetStatusLineHighlight()
augroup END

fun! s:SetStatusLineHighlight() "{{{
if (has("gui_running") && &background == "dark") || g:colors_name == "ambient" || g:colors_name == "luciusmod"

    " luciusmod
        "hi clear StatusLine
        "hi clear StatusLineNC

        hi StatusLine term=bold cterm=bold,underline ctermfg=114 ctermbg=238 gui=bold guifg=#76D787 guibg=#444444
        hi StatusLineNC term=NONE cterm=NONE,underline ctermfg=244 ctermbg=238 guifg=#777777 guibg=#444444

	hi StatusLineFn	        ctermbg=238 ctermfg=50  cterm=bold,underline
	hi StatusLineFlags	ctermbg=238 ctermfg=208 cterm=bold,underline
        hi StatusLineFnNC	ctermbg=238 ctermfg=30  cterm=NONE,underline
	hi StatusLineFlagsNC	ctermbg=238 ctermfg=100 cterm=NONE,underline

	hi StatusLineFn		gui=bold guibg=#444444 guifg=#00FFD7
	hi StatusLineFlags	gui=bold guibg=#444444 guifg=#FF8700
	hi StatusLineFnNC	gui=NONE guibg=#444444 guifg=#008781
	hi StatusLineFlagsNC	gui=NONE guibg=#444444 guifg=#878700
elseif (has("gui_running") && &background == "light") || g:colors_name == "peakseamod"
    " peakseamod
        hi clear StatusLine
        hi clear StatusLineNC
        hi StatusLine term=bold cterm=NONE ctermfg=130 ctermbg=142 gui=bold guifg=fg guibg=#a6caf0
        hi StatusLineNC term=NONE cterm=NONE ctermfg=245 ctermbg=150 guifg=#5f5f5f guibg=#a6caf0

        " Old version
        " hi StatusLineFn 	ctermbg=114 ctermfg=160	cterm=bold
        " hi StatusLineFlags 	ctermbg=114 ctermfg=161	cterm=NONE
        " hi StatusLineFnNC	ctermbg=114 ctermfg=172 cterm=NONE
        " hi StatusLineFlagsNC	ctermbg=114 ctermfg=197 cterm=NONE

        hi StatusLineFn	ctermbg=184 ctermfg=162 cterm=bold
        hi StatusLineFlags	ctermbg=183 ctermfg=54	cterm=NONE
        hi StatusLineFnNC	ctermbg=150 ctermfg=162 cterm=NONE
        hi StatusLineFlagsNC	ctermbg=150 ctermfg=195 cterm=NONE

	hi StatusLineFn		guibg=#A6CAF0 gui=bold guifg=#d70000
	hi StatusLineFlags	guibg=#A6CAF0 gui=NONE guifg=#d7005f
	hi StatusLineFnNC	guibg=#A6CAF0 gui=NONE guifg=#d78700
	hi StatusLineFlagsNC	guibg=#A6CAF0 gui=NONE guifg=#d7875f
else
	hi link StatusLineFn StatusLine
	hi link StatusLineFlags StatusLine
	hi link StatusLineFnNC StatusLineNC
	hi link StatusLineFlagsNC StatusLineNC
endif
        hi link VertSplit StatusLine
        hi VertSplit cterm=NONE

endfunction "}}}

fun! StatusLineRealSyn() "{{{
    let synId = synID(line('.'),col('.'),1)
    let realSynId = synIDtrans(synId)
    if synId == realSynId
        return 'Normal'
    else
        return synIDattr( realSynId, 'name' )
    endif
endfunction "}}}

fun! s:SetStatusline(mode) "{{{
  setlocal statusline=
  if (a:mode == "active")
	  setlocal statusline+=%#StatusLineFn#%F
	  setlocal statusline+=%#StatusLineFlags#%m%r%h%w%0*\ 
  else
	  setlocal statusline+=%#StatusLineFnNC#%F
	  setlocal statusline+=%#StatusLineFlagsNC#%m%r%h%w%0*\ 
  endif
  setlocal statusline+=%<[%n]\ [%{&ff}]\ [%{&fenc}]\ %y
  setlocal statusline+=%=
  if (a:mode == "active")
	  setlocal statusline+=%{synIDattr(synID(line('.'),col('.'),1),'name')}\  
	  setlocal statusline+=%-15{StatusLineRealSyn()}
  endif
  setlocal statusline+=%03.3b\ x\%02.2B\ \ \ line:\ %-4l\ col:\ %-5(%c%V%)\ \ %P\ of\ %L\ lines
endfunction "}}}

"set statusline=%<%F%h%m%r%h%w%y\ %{&ff}\ %{strftime(\"%d/%m/%Y-%H:%M\")}%=\ col:%c%V\ ascii:%b\ pos:%o\ lin:%l\,%L\ %P
"set statusline=%F%m%r%h%w\ (%{&ff}){%Y}[%l,%v][%p%%]\ %{strftime(\"%d/%m/%y\ -\ %H:%M\")}

"}}}

if &background=="light"
	colorscheme peakseamod
else
	"colorscheme luciusmod
        "colorscheme saturn_csa
        colorscheme ambient
endif

"""" }}}
"""" {{{ CUSTOM MAPPINGS

let mapleader = ","

if has("win32")
nmap <Leader>S :source $VIM/_vimrc<CR>
nmap <Leader>E :e $VIM/vim_repo/.vimrc<CR>
else
nmap <Leader>S :source ~/.vimrc<CR>
nmap <Leader>E :e ~/.vimrc<CR>
endif


nmap <Leader>n :cnext<CR>
nmap <Leader>p :cprev<CR>
nmap <Leader><Leader> :cc<CR>
"nmap <Leader>/ :set invhlsearch<CR>
nmap <Leader>e :e %:p:h<CR>
nmap <Leader>w :w<CR>

"nmap <Space> za
"nmap <Space> :noh<CR>

augroup MakeSpaceMap
	au! MakeSpaceMap
	au   FileType         prolog,erlang,haskell   nmap   <buffer><silent>  <Leader><Space>   :w<CR>:silent make!<CR>:redraw!<CR>
	au   FileType                         c,cpp   nmap   <buffer><silent>  <Leader><Space>   :call g:ClangUpdateQuickFix()<CR>
	au   FileType                     tex,latex   nmap   <buffer><silent>  <Leader><Space>   :MakeLatex<CR>:silent ShowErrors<CR>
	au   FileType                        python   nmap   <buffer><silent>  <Leader><Space>   :!python %<CR>
augroup END

nmap <Leader>T2 :set ts=2<CR>
nmap <Leader>T4 :set ts=4<CR>
nmap <Leader>T8 :set ts=8<CR>

nmap <Leader>t2 :set sts=2 sw=2<CR>
nmap <Leader>t4 :set sts=4 sw=4<CR>
nmap <Leader>t8 :set sts=8 sw=8<CR>


""" Idea taken from tabbar.vim
function! <SID>Bf_SwitchTo( bufNum)
    let l:vimbuf = a:bufNum
    if bufexists(l:vimbuf) == 1
	exec "b!" . l:vimbuf
    endif
endfunction

if has('gui_running')
    noremap <script><silent> <M-1> :call <SID>Bf_SwitchTo( 1)<CR>:<BS>
    noremap <script><silent> <M-2> :call <SID>Bf_SwitchTo( 2)<CR>:<BS>
    noremap <script><silent> <M-3> :call <SID>Bf_SwitchTo( 3)<CR>:<BS>
    noremap <script><silent> <M-4> :call <SID>Bf_SwitchTo( 4)<CR>:<BS>
    noremap <script><silent> <M-5> :call <SID>Bf_SwitchTo( 5)<CR>:<BS>
    noremap <script><silent> <M-6> :call <SID>Bf_SwitchTo( 6)<CR>:<BS>
    noremap <script><silent> <M-7> :call <SID>Bf_SwitchTo( 7)<CR>:<BS>
    noremap <script><silent> <M-8> :call <SID>Bf_SwitchTo( 8)<CR>:<BS>
    noremap <script><silent> <M-9> :call <SID>Bf_SwitchTo( 9)<CR>:<BS>
    noremap <script><silent> <M-0> :call <SID>Bf_SwitchTo( 10)<CR>:<BS>
else
    noremap <script><silent> 1 :call <SID>Bf_SwitchTo( 1)<CR>:<BS>
    noremap <script><silent> 2 :call <SID>Bf_SwitchTo( 2)<CR>:<BS>
    noremap <script><silent> 3 :call <SID>Bf_SwitchTo( 3)<CR>:<BS>
    noremap <script><silent> 4 :call <SID>Bf_SwitchTo( 4)<CR>:<BS>
    noremap <script><silent> 5 :call <SID>Bf_SwitchTo( 5)<CR>:<BS>
    noremap <script><silent> 6 :call <SID>Bf_SwitchTo( 6)<CR>:<BS>
    noremap <script><silent> 7 :call <SID>Bf_SwitchTo( 7)<CR>:<BS>
    noremap <script><silent> 8 :call <SID>Bf_SwitchTo( 8)<CR>:<BS>
    noremap <script><silent> 9 :call <SID>Bf_SwitchTo( 9)<CR>:<BS>
    noremap <script><silent> 0 :call <SID>Bf_SwitchTo( 10)<CR>:<BS>
endif



"""" }}}
"""" {{{ PLUGIN SETTINGS AND MAPPINGS

"""" {{{ Eclim
let g:EclimDisabled=1
"""" }}}

"""" {{{ Rainbow
let g:rainbow_active=0
map <silent> <Leader>R :RainbowToggle<cr>

"""" }}}

"""" {{{ ClangComplete
let g:clang_use_library=1
"let g:clang_library_path="/home/students/inf/m/ml248350/lib64/"
let g:clang_library_path="/usr/lib/llvm/"
let g:clang_periodic_quickfix=0
let g:clang_complete_macros=1
let g:clang_complete_auto=0
"""" }}}

"""" {{{ Easytags
let g:easytags_by_filetype="~/tags/"
let g:easytags_dynamic_files=1

let g:easytags_on_cursorhold=1
let g:easytags_updatetime_min=4000
let g:easytags_updatetime_autodisable=1
let g:easytags_include_members=1
let g:easytags_resolve_links=1


hi! link cTypeTag Special
hi! link cMemberTag None

" - C: 'cTypeTag', 'cEnumTag', 'cPreProcTag', 'cFunctionTag', 'cMemberTag'
"""" }}}


if has("gui") == 0
    let g:CSApprox_verbose_level=0
endif 

" FShwitch
map <silent> <Leader>hh :FSHere<cr>
map <silent> <Leader>hs :FSSplitBelow<cr>
map <silent> <Leader>hv :FSSplitRight<cr>

map <silent> <Leader>hr :FSRight<cr>
map <silent> <Leader>hl :FSLeft<cr>
map <silent> <Leader>ha :FSAbove<cr>
map <silent> <Leader>hb :FSBelow<cr>
""" ATP Latex plugin
let g:atp_SetMathVimOptions = 0

let g:atp_callback=0
let g:atp_status_notification=0

""" Lets disable cctree, its too slow for kernel
let loaded_cctree = 1
""" This one does not seem to work well with snipMate
let loaded_search_complete = 1

""" c / cpp
let g:c_gnu=1
let g:c_curly_error=1

set cinoptions=g0,:0,l1,t0

""" Erlang
let g:erlangCompletionGrep = 'bzgrep'
let g:erlangManPath = '/usr/lib/erlang/man/'
let g:erlangManSuffix = '.bz2'

""" Haskell mode and syntax
au BufNewFile,BufEnter *.hs compiler ghc
let g:haddock_browser="/usr/bin/firefox"
let hs_highlight_delimiters=1
" let hs_highlight_types=1
" let hs_highlight_more_types=1
let hs_highlight_debug=1

""" Tlist options
let Tlist_Auto_Highlight_Tag=1
let Tlist_Compact_Format=1
let Tlist_Highlight_Tag_On_BufEnter=1
let Tlist_Display_Prototype=0
let Tlist_Enable_Fold_Column=0
let Tlist_Highlight_Tag_On_BufEnter=1
"let Tlist_Use_Right_Window=1

""" Tagbar options
let g:tagbar_compact = 1
let g:tagbar_usearrows = 1
let g:tagbar_autoshowtag = 1

""" NERDTree
let NERDTreeWinPos="right"
let NERDChristmasTree=1
let NERDTreeHighlightCursorline=1
let NERDTreeHijackNetrw=1
let NERDTreeQuitOnOpen=1


""" srcexpl options
let g:SrcExpl_isUpdateTags = 0
let g:SrcExpl_pluginList = [
	\ "__Tag_List__",
        \ "__Tagbar__",
	\ "NERD_tree_1", "NERD_tree_2", "NERD_tree_3", "NERD_tree_4",
	\ "Source_Explorer",
	\ "-MiniBufExplorer-"
\ ]
let g:SrcExpl_gobackKey = ''
let g:SrcExpl_refreshTime = 1000


let g:netrw_browse_split=0

"let g:compiler_gcc_ignore_unmatched_lines=1


"
" bufkill
"

"
" Project
"
nmap <silent> <leader>P :Project<CR>
nmap <silent> <C-W>a <Plug>ToggleProject

"
" BufExplorer
"
let g:bufExplorerDetailedHelp=1      " Show detailed help.
let g:bufExplorerSortBy='number'     " Sort by the buffer's number.
" To control weither or not to show buffers on for the specific tab or not, use: >
let g:bufExplorerShowTabBuffer=1        " Yes.


"
" MiniBufExplorer
"
let g:miniBufExplSplitToEdge = 1
let g:miniBufExplModSelTarget = 1
let g:miniBufExplMaxSize = 1
"let g:miniBufExplorerMoreThanOne = 1

hi link MBEVisibleChanged Boolean
hi link MBEVisibleNormal Boolean

hi link MBEVisibleActive String
hi link MBEVisibleChangedActive String

hi link MBENormal Comment
hi link MBEChanged Comment

" MBENormal      xxx links to Comment
" MBEChanged     xxx links to String
" MBEVisibleNormal xxx links to Special
" MBEVisibleChanged xxx links to Special
" MBEVisibleActive xxx links to Boolean
" MBEVisibleChangedActive xxx links to Error
"
" NERD commenter
"
let NERDBlockComIgnoreEmpty=0
let NERDCommentWholeLinesInVMode=2
let NERDSpaceDelims=1
" preferr /* to // in cpp
let NERD_cpp_alt_style=1

"
" Command-t
"
" Its very unstable
"let g:CommandTMatchWindowAtTop=1
"nmap <silent> <Leader>bo :CommandT<CR>
"nmap <silent> <Leader>bO :CommandT expand("%:p:h")<CR>

"
" errormarker
"
nmap <silent> <Leader>C :ErrorAtCursor<CR>
let errormarker_errortext = ">>"
let errormarker_warningtext= ">>"

let errormarker_warninggroup = "NONE"
"let errormarker_errorgroup = "SpellBad"
let errormarker_errorgroup = "NONE"
let errormarker_warningtypes = "wWiI"

"
" Mark plugin
" 
nmap <silent> <Leader>m <Plug>MarkSet
vmap <silent> <Leader>m <Plug>MarkSet
nmap <silent> <Leader>r <Plug>MarkRegex
vmap <silent> <Leader>r <Plug>MarkRegex
nmap <silent> <Leader>M <Plug>MarkClear
" No default mapping for <Plug>MarkAllClear. 

nmap <silent> <Leader>* <Plug>MarkSearchCurrentNext
nmap <silent> <Leader># <Plug>MarkSearchCurrentPrev
nmap <silent> <Leader>/ <Plug>MarkSearchAnyNext
nmap <silent> <Leader>? <Plug>MarkSearchAnyPrev
nmap <silent> * <Plug>MarkSearchNext
nmap <silent> # <Plug>MarkSearchPrev

"
" FuzzyFinder
" 
let g:fuf_modesDisable = []
let g:fuf_mrufile_maxItem = 400
let g:fuf_mrucmd_maxItem = 400
nnoremap <Leader>fj     :FufBuffer<CR>
nnoremap <Leader>fk     :FufFileWithCurrentBufferDir<CR>
nnoremap <Leader>fK     :FufFileWithFullCwd<CR>
nnoremap <Leader>f<C-k> :FufFile<CR>
nnoremap <Leader>fl     :FufCoverageFileChange<CR>
nnoremap <Leader>fL     :FufCoverageFileChange<CR>
nnoremap <Leader>f<C-l> :FufCoverageFileRegister<CR>
nnoremap <Leader>fd     :FufDirWithCurrentBufferDir<CR>
nnoremap <Leader>fD     :FufDirWithFullCwd<CR>
nnoremap <Leader>f<C-d> :FufDir<CR>
nnoremap <Leader>fn     :FufMruFile<CR>
nnoremap <Leader>fN     :FufMruFileInCwd<CR>
nnoremap <Leader>fm     :FufMruCmd<CR>
nnoremap <Leader>fu     :FufBookmarkFile<CR>
nnoremap <Leader>f<C-u> :FufBookmarkFileAdd<CR>
vnoremap <Leader>f<C-u> :FufBookmarkFileAddAsSelectedText<CR>
nnoremap <Leader>fi     :FufBookmarkDir<CR>
nnoremap <Leader>f<C-i> :FufBookmarkDirAdd<CR>
nnoremap <Leader>ft     :FufTag<CR>
nnoremap <Leader>fT     :FufTag!<CR>
nnoremap <Leader>f<C-]> :FufTagWithCursorWord!<CR>
nnoremap <Leader>f,     :FufBufferTag<CR>
nnoremap <Leader>f<     :FufBufferTag!<CR>
vnoremap <Leader>f,     :FufBufferTagWithSelectedText!<CR>
vnoremap <Leader>f<     :FufBufferTagWithSelectedText<CR>
nnoremap <Leader>f}     :FufBufferTagWithCursorWord!<CR>
nnoremap <Leader>f.     :FufBufferTagAll<CR>
nnoremap <Leader>f>     :FufBufferTagAll!<CR>
vnoremap <Leader>f.     :FufBufferTagAllWithSelectedText!<CR>
vnoremap <Leader>f>     :FufBufferTagAllWithSelectedText<CR>
nnoremap <Leader>f]     :FufBufferTagAllWithCursorWord!<CR>
nnoremap <Leader>fg     :FufTaggedFile<CR>
nnoremap <Leader>fG     :FufTaggedFile!<CR>
nnoremap <Leader>fo     :FufJumpList<CR>
nnoremap <Leader>fp     :FufChangeList<CR>
nnoremap <Leader>fq     :FufQuickfix<CR>
nnoremap <Leader>fy     :FufLine<CR>
nnoremap <Leader>fh     :FufHelp<CR>
nnoremap <Leader>fe     :FufEditDataFile<CR>
nnoremap <Leader>fr     :FufRenewCache<CR>
"FuzzyFinder

"""" }}}
"""" vim:foldmethod=marker sts=4 sw=4 ts=8 et:
